
# TechuBank environment

## Environment variables for web application
These variables are in *app/config/{environment}.json*

* techubank_api_url (ej. https://localhost:3443)
* techubank_auth_server_url (ej. https://localhost:4443)


## Environment variables for backend components
It is forbidden to store any kind of credential in your repository, and the best way to pass your credentials to the components is via environment variables.

First, let's explain the set of variables TechUBank manages.

* *ENV*: Choose the enviornment to use (dev, local, docker, kubernetes, pro). When using docker-compose you have to use *docker* value
* *MLAB_USERNAME*: Username with grants in [mlab mongodb](https://mlab.com/databases/techubank-jcmdrv)
* *MLAB_PASSWORD*: User's password
* *MLAB_HOST*: Host ant port to Mongo
* *MLAB_DATABASENAME*: Mongo Database name
* *API_USERNAME*: Username with *admin* grants in *users* collection
```
{
    "scopes": [
        "admin"
    ],
    "username": "techubank",
    "name": "techubank",
    "first_name": "techubank",
    "last_name": "techubank",
    "nif": "12345676y",
    "birth_date": "01/02/2019",
    "phone": "+34911234567",
    "email": "d.d@d.es",
    "password": "****************************",
    "address": {
        "street": "techubank",
        "zip_code": "techubank",
        "city": "techubank",
        "country": "techubank"
    },
    "register_date": {
        "$date": "2018-11-21T21:06:00.264Z"
    },
    "status": "ACTIVE"
}
```
* API_PASSWORD: Admin user's password

These variables must be stored in a *.env* file on the basepath where you launch the component. You will find the details below.

* [techubank-api](https://bitbucket.org/jcmdrv/techubank-api/src/develop/): Rest server consumed by techubank-web
    * *API_USERNAME*
    * *API_PASSWORD*
* [techubank-auth-server](https://bitbucket.org/jcmdrv/techubank-auth-server/src/develop/): OAuth2 server
    * *MLAB_USERNAME*
    * *MLAB_PASSWORD*
    * *MLAB_HOST*
    * *MLAB_DATABASENAME*
* [techubank-mlab](https://bitbucket.org/jcmdrv/techubank-mlab/src/develop/): Isolate layer for storing objects into MongoDB. It encapsulates the different operations TechuBank wants to open over an API Rest.
    * *MLAB_USERNAME*
    * *MLAB_PASSWORD*
    * *MLAB_HOST*
    * *MLAB_DATABASENAME*
* [techubank-compose](https://bitbucket.org/jcmdrv/techubank-compose/src/develop/): Orchestrate all the components in the launching process.
    * *ENV*: docker  (important)
    * *MLAB_USERNAME*
    * *MLAB_PASSWORD*
    * *MLAB_HOST*
    * *MLAB_DATABASENAME*
    * *API_USERNAME*
    * *API_PASSWORD*

{#self-signed-certificate}
## HTTPS settings
Because this is a project for study purposes we have enabled both protocols (http, https).

It's neccessary to setup your certificates when using https. We have been working with a self-signed certificate.

When you work with a self-signed certificate web applications will block requests on first try, waiting for your reactive action to accept the self-signed certificate.

### Setup https
Create an self-signed SSL certificate

* Write down the Common Name (CN) for your SSL Certificate. The CN is the fully qualified name for the system that uses the certificate. If you are using Dynamic DNS, your CN should have a wild-card, for example: *.api.com. Otherwise, use the hostname or IP address set in your Gateway Cluster (for example. 192.16.183.131 or dp1.acme.com).

* Run the following OpenSSL command to generate your private key and public certificate. Answer the questions and enter the Common Name when prompted.

```
$ openssl req -newkey rsa:2048 -nodes -keyout key.pem -x509 -days 365 -out certificate.pem


Country Name (2 letter code) [AU]:localhost
string is too long, it needs to be no more than 2 bytes long
Country Name (2 letter code) [AU]:ES
State or Province Name (full name) [Some-State]:MADRID
Locality Name (eg, city) []:MADRID
Organization Name (eg, company) [Internet Widgits Pty Ltd]:techubank
Organizational Unit Name (eg, section) []:techubank
Common Name (e.g. server FQDN or YOUR name) []:techubank
Email Address []:techubank@techu.com

```
* key.pem is the private key
* certificate.pem is the self-signed certificate

* Copy the pem files on the basepath 

```
$ techubank-auth-server > ls -lrt
drwxrwxr-x   2 techu techu    4096 oct 16 10:43 bin
-rw-rw-r--   1 techu techu     180 oct 19 08:28 Dockerfile
-rw-r--r--   1 techu techu     680 oct 30 10:32 sonar-scanner.properties
drwxrwxr-x   2 techu techu    4096 oct 30 16:24 test
-rw-rw-r--   1 techu techu    2396 nov  6 09:26 bitbucket-pipelines.yml
drwxrwxr-x   3 techu techu    4096 nov 12 07:59 src
-rw-rw-r--   1 techu techu    1633 nov 12 07:59 package.json
drwxrwxr-x   2 techu techu    4096 nov 12 07:59 ws-doc
-rw-------   1 techu techu    1708 nov 12 09:50 key.pem
-rw-rw-r--   1 techu techu    1436 nov 12 09:51 certificate.pem
drwxrwxr-x 523 techu techu   20480 nov 12 09:53 node_modules
-rw-rw-r--   1 techu techu    6483 nov 12 09:54 README.md

```

References: 
* https://www.sitepoint.com/how-to-use-ssltls-with-node-js/
* https://www.ibm.com/support/knowledgecenter/en/SSWHYP_4.0.0/com.ibm.apimgmt.cmc.doc/task_apionprem_gernerate_self_signed_openSSL.html

# Localhost - docker-compose

1) Clone *techubank-compose* project and checkout develop branch.
```
$ git clone git clone git@bitbucket.org:jcmdrv/techubank-compose.git
$ git checkout develop
$ cd techubank-compose
```

2) Setup environment variables values in a *.env* file located at docker-compose.yaml level
```
ENV=docker
MLAB_USERNAME=<mlab_username>
MLAB_PASSWORD=<mlab_password>
MLAB_HOST=<mlab_host>
MLAB_DATABASENAME=<mlab_databasename>
API_USERNAME=<admin_username>
API_PASSWORD=<admin_password>
```

3) Setup your certificate (key.pem and certificate.pem)
Our componentes are going to be launched listening both protocols (http and htttps), so you need to setup propperly at least a self-signed certificate.

Copy key.pem and certificate.pem on the basepath.

[How to create a self-signed certificate]('#self-signed-certificate')

4) Be sure to fetch latest containers version
```
$ docker-compose pull
```

5) Launch the ecosystem
```
$ docker-compose up
```

6) [Open TechuBank WEB](http://localhost:8001/index.html)

